## This library is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public License
## along with this library.  If not, see <http://www.gnu.org/licenses/>.
##

## the make file provide rule to compile and flash firmware for STM32F1 micro-controllers
## it uses libopencm3

# be silent per default, but 'make V=1' will show all compiler calls.
ifneq ($(V),1)
Q := @
NULL := 1> /dev/null 2> /dev/null
endif

# the final binary name (without extension)
BINARY = firmware

# which development board is used
# supported are: SYSTEM_BOARD, MAPLE_MINI, BLUE_PILL, CORE_BOARD
BOARD = CORE_BOARD

# source files
CSRC = $(wildcard *.c)
CHDR = $(wildcard *.h)
OBJ = $(patsubst %.c,%.o,$(CSRC))

# my library collection
LIB = lib
# the library files to use (this will be populated using includes based DEPENDENCIES)
LIB_CSRC =
LIB_CHDR = $(patsubst %.c,%.h,$(LIB_CSRC))
LIB_OBJ = $(patsubst %.c,%.o,$(LIB_CSRC))

# figure out based on the includes which library files are used in the main CSRC files
DEPENDENCIES = $(patsubst %.c,%.inc,$(CSRC))
# populates LIB_CSRC based on the library files used
-include $(DEPENDENCIES)

# executables for linking, compiling, debugging, ...
# use ELLCC cross-compiling chain (based on clang/llvm + musl)
PREFIX := arm-none-eabi
# use ELLCC as compile
CC := $(PREFIX)-gcc
LD := $(PREFIX)-ld
LD := $(PREFIX)-ld
AR := $(PREFIX)-ar
AS := $(PREFIX)-as
OBJCOPY := $(PREFIX)-objcopy
OBJDUMP := $(PREFIX)-objdump
# ecc-gdb is buggy and crashes on "kill"
GDB := arm-none-eabi-gdb

# opencm3 libraries
OPENCM3_DIR := libopencm3
OPENCM3_INC = $(OPENCM3_DIR)/include
OPENCM3_LIB = $(OPENCM3_DIR)/lib
# library for the STM32F1 (provided by opencm3)
STM32F1_LIB = opencm3_stm32f1

# linker script for STN32F1 boards
ifeq ($(BOARD),SYSTEM_BOARD)
LDSCRIPT = $(OPENCM3_DIR)/lib/stm32/f1/stm32f103x8.ld
else ifeq ($(BOARD),BLUE_PILL)
LDSCRIPT = $(OPENCM3_DIR)/lib/stm32/f1/stm32f103x8.ld
else ifeq ($(BOARD),CORE_BOARD)
LDSCRIPT = $(OPENCM3_DIR)/lib/stm32/f1/stm32f103x8.ld
else ifeq ($(BOARD),MAPLE_MINI)
LDSCRIPT = $(OPENCM3_DIR)/lib/stm32/f1/stm32f103xb.ld
endif

# device micro-controller and board
DEFS += -DSTM32F1 -D$(BOARD)

# C flags
# optimize for size
CFLAGS += -Os
# add debug symbols (remove for smaller release)
CFLAGS += -ggdb
# use C99 (supported by most an sufficient)
CFLAGS += -std=c99
# have strict warning (for better code)
CFLAGS += -Wpedantic -Wall -Werror -Wundef -Wextra -Wshadow -Wredundant-decls -Wmissing-prototypes -Wstrict-prototypes -Wstrict-overflow=5
# add options for better code optimization
CFLAGS += -fno-common -ffunction-sections -fdata-sections
# use variable size enum (opencm3, gcc, and compiler-rt do)
CFLAGS += -fshort-enums
# use no variable size enum (ELLCC/musl does not)
#CFLAGS += -fno-short-enums
# don't use system main definition (the starting point)
CFLAGS += -ffreestanding
# don't use the standard library
#CFLAGS += -nostdlib -nostdinc
# include ELLCC libraries
#CFLAGS += -I$(ELLCC)libecc/include/ -I$(ELLCC)libecc/include/arm/
# include own libraries 
CFLAGS += -I . $(patsubst %,-I%,$(LIB))
# include opencm3 libraries
CFLAGS += -I $(OPENCM3_INC)
# add defines for micro-controller and board
CFLAGS += $(DEFS)

# linker flags
# build static binary (no shared libraries on the micro-controller)
LDFLAGS += -static
# don's include the system start files
LDFLAGS += -nostartfiles
# only keep used sections
LDFLAGS += --gc-sections
# don't use system libraries
LDFLAGS += -nostdlib -nostdinc
# add ELLCC standard libraries (for libc, libm, libgcc)
LDFLAGS += --library-path /usr/arm-none-eabi/lib/armv7-m/
LDFLAGS += --library-path /usr/lib/arm-none-eabi/lib/armv7-m/
LDFLAGS += --library-path /usr/lib/gcc/arm-none-eabi/*/armv7-m/
#LDFLAGS += --library-path $(ELLCC)libecc/lib/cortex-m3-linux/
# opencm3 libraries
LDFLAGS += --library-path $(OPENCM3_LIB)
# linker script with definitions for micro-controller
LDFLAGS += --script $(LDSCRIPT)
# used libraries (gcc provides the ARM ABI, not sure how to replace with compiler-rt)
LDLIBS += --library $(STM32F1_LIB) --library c --library m --library gcc

# target micro-controller information (ARM Cortex-M3 supports thumb and thumnb2, but does not include a floating point unit)
ARCH_FLAGS = -mthumb -mcpu=cortex-m3 -msoft-float

# SWD adapter used
# supported are : st-link v2 (STLINKV2), black magic probe (BMP)
SWD_ADAPTER ?= BMP
ifeq ($(SWD_ADAPTER),STLINKV2)
# OpenOCD configuration
OOCD ?= openocd
OOCD_INTERFACE ?= stlink-v2
OOCD_TARGET ?= stm32f1x
else ifeq ($(SWD_ADAPTER),BMP)
# the black magic probe has a SWD controller built in
BMPPORT ?= /dev/ttyACM0
endif

# which USB CDC ACM port is used bu the device, so we can reset it
ifeq ($(SWD_ADAPTER),STLINKV2)
ACMPORT = /dev/ttyACM0
else ifeq ($(SWD_ADAPTER),BMP)
ACMPORT = /dev/ttyACM2
endif
ACMPORT_EXISTS = $(shell [ -e $(ACMPORT) ] && echo 1 || echo 0 )

# compile target rules
all: elf

elf: $(BINARY).elf
bin: $(BINARY).bin
hex: $(BINARY).hex
srec: $(BINARY).srec
list: $(BINARY).list

%.bin %.hex %.srec: %.elf
	$(Q)$(OBJCOPY) -Osrec $(<) $(@)

%.map %.list: %.elf
	$(Q)$(OBJDUMP) -S $(<) > $(@)

%.elf: $(OPENCM3_LIB)/lib$(STM32F1_LIB).a $(OBJ) $(LIB_OBJ)
	$(info linking $(@))
	$(Q)$(LD) $(LDFLAGS) $(OBJ) $(LIB_OBJ) $(LDLIBS) -o $(@)
	$(Q)size $(@)

%.o: %.c $(CHDR) $(LIB_CHDR)
	$(info compiling $(@))
	$(Q)$(CC) $(CFLAGS) $(ARCH_FLAGS) -o $(@) -c $(<)

# generate dependencies
%.d: %.c
	@# check which libraries are used
	$(Q)$(CC) $(CFLAGS) $(ARCH_FLAGS) -MM -MF $(@) -o /dev/null -c $(<)

# figure out which library source files are used for later inclusion
%.inc: %.d
	$(Q)grep -o -e " ${LIB}\/[^ ]*\.h" $(<) | sed -e 's|\(.*\)\.h$$|LIB_CSRC +=\1.c\n-include\1.inc|g' -e 's|.*${*}.*||g' > $(@)

# doxygen documentation
doc: Doxyfile README.md $(CSRC) $(CHDR) $(LIB_CSRC) $(LIB_CHDR)
	$(Q)doxygen $(<)

clean:
	$(Q)$(RM) $(BINARY).elf $(BINARY).bin $(BINARY).hex $(BINARY).map $(OBJ) $(LIB_OBJ) $(LIB)/*.o $(DEPENDENCIES) $(LIB)/*.inc

# make libopencm3 if library for STM32F1 is not yet existing
$(OPENCM3_LIB)/lib$(STM32F1_LIB).a:
	$(info compiling libopencm3 library)
	git submodule init
	git submodule update
	$(Q)$(MAKE) -C $(OPENCM3_DIR)

flash: $(BINARY).hex
	$(info flashing $(<) using SWD)
ifeq ($(SWD_ADAPTER),STLINKV2)
	$(Q)$(OOCD) --file interface/$(OOCD_INTERFACE).cfg --file target/$(OOCD_TARGET).cfg --command "init" --command "reset init" --command "flash write_image erase $(<)" --command "reset" --command "shutdown" $(NULL)
else ifeq ($(SWD_ADAPTER),BMP)
	$(Q)$(GDB) --eval-command="target extended-remote $(BMPPORT)" --eval-command="set confirm off" --eval-command="kill" --eval-command="monitor swdp_scan" --eval-command="attach 1" --eval-command="load" --eval-command="kill" --eval-command="detach" --eval-command="quit" $(<)
endif

# reset device by setting the data width to 5 bis on the USB CDC ACM port
reset:
ifeq ($(ACMPORT_EXISTS), 1)
	$(Q)stty --file $(ACMPORT) 115200 raw cs5
	$(Q)sleep 0.5
endif

# debug using GDB
debug: $(BINARY).elf
ifeq ($(SWD_ADAPTER),STLINKV2)
	# for GDB to work with openOCD the firmware needs to be reloaded
	$(Q)$(GDB) --eval-command="target remote | $(OOCD) --file interface/$(OOCD_INTERFACE).cfg --file target/$(OOCD_TARGET).cfg --command \"gdb_port pipe; log_output /dev/null; init\"" --eval-command="monitor reset halt" --eval-command="load" --eval-command="monitor reset init" $(<)
else ifeq ($(SWD_ADAPTER),BMP)
	$(Q)$(GDB) --eval-command="target extended-remote $(BMPPORT)" --eval-command="monitor version" --eval-command="monitor swdp_scan" --eval-command="attach 1" $(<)
endif

.PHONY: clean elf bin hex srec list flash reset
