/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** CuVoodoo clapperboard firmware (for STM32F103Cx micro-controller)
 *  @file main.c
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016-2018
 */

/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdbool.h> // boolean type
#include <string.h> // string utilities

/* STM32 (including CM3) libraries */
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities
#include <libopencm3/cm3/scb.h> // vector table definition
#include <libopencm3/cm3/nvic.h> // interrupt utilities
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/exti.h> // external interrupt utilities
#include <libopencm3/stm32/rtc.h> // real time clock utilities
#include <libopencm3/stm32/iwdg.h> // independent watchdog utilities
#include <libopencm3/stm32/dbgmcu.h> // debug utilities
#include <libopencm3/stm32/flash.h> // flash utilities
#include <libopencm3/stm32/timer.h> // timer utilities

/* own libraries */
#include "global.h" // board definitions
#include "print.h" // printing utilities
#include "usart.h" // USART utilities
#include "usb_cdcacm.h" // USB CDC ACM utilities
#include "rtc_ds1307.h" // DS1307 RTC utilities
#include "led_tm1637.h" // TM1637 7-segment controller utilities
#include "led_max7219.h" // MAX7219 7-segment controller utilities

#define WATCHDOG_PERIOD 10000 /**< watchdog period in ms */

/** @defgroup main_flags flag set in interrupts to be processed in main loop
 *  @{
 */
volatile bool rtc_tick_flag = false; /**< flag set when RTC ticked */
volatile bool frame_flag = false; /**< flag set when a frame has passed */
volatile bool keep_alive_flag = false; /**< flag to restart shutdown counter on power switch activity */
volatile bool clap_flag = false; /**< flag set on clap */
/** @} */

#define POWER_SWITCH_PORT B /**< port to switch power of all devices (including this micro-controller) */
#define POWER_SWITCH_PIN 8 /**< pin to switch power of all devices (including this micro-controller) */
#define POWER_BUTTON_PORT B /**< port to detect power switching activity by shaking (to keep alive) */
#define POWER_BUTTON_PIN 1 /**< pin to detect power switching activity by shaking (to keep alive) */

#define SQUARE_WAVE_PORT B /**< port connected to RTC DS1307 square wave output */
#define SQUARE_WAVE_PIN 0 /**< pin connected to RTC DS1307 square wave output */
volatile uint8_t rtc_seconds = 0; /**< number of seconds passed, incremented by the square wave */

#define STANDBY_TIMEOUT 30  /**< number of seconds after last shake before going down */
volatile uint16_t standby_timer = 0; /**< number of seconds since last power-up activity (using shake sensor) */

#define FRAME_TIMER 2 /**< timer to count frame time */
#define FRAME_RATE 25 /**< frame rate in frames per second */
volatile uint8_t frame_count = 0; /**< number of frames passed */

#define BUZZER_TIMER 1 /**< timer to generate scene and take count */
#define BUZZER_1_PORT A /**< use timer channel 1 (and it's negative) to driver buzzer */
#define BUZZER_1_PIN 7 /**< use timer channel 1 (and it's negative) to driver buzzer */
#define BUZZER_2_PORT A /**< use timer channel 1 (and it's negative) to driver buzzer */
#define BUZZER_2_PIN 8 /**< use timer channel 1 (and it's negative) to driver buzzer */

#define MUX_EN_PORT B /**< port to enable multiplexer */
#define MUX_EN_PIN 9 /**< pin to enable multiplexer */
#define MUX_S0_PORT B /**< port to select multiplexer output */
#define MUX_S0_PIN 3 /**< pin to select multiplexer output */
#define MUX_S1_PORT B /**< port to select multiplexer output */
#define MUX_S1_PIN 4 /**< pin to select multiplexer output */
#define MUX_S2_PORT B /**< port to select multiplexer output */
#define MUX_S2_PIN 5 /**< pin to select multiplexer output */

/** number to be display and changed using the buttons */
struct number_t {
	uint16_t number; /**< number to display (0-9999, 0xffff=off) */
	uint8_t up; /**< which of the 16 buttons is to increment the number */
	uint8_t down; /**< which of the 16 buttons is to decrement the number */
	uint8_t display; /**< which display on the channel multiplexer */
};
/** episode, scene, take, video 1, audio 1, video 2, audio 2 number (in this order) */
struct number_t numbers[] = {
	{0, 0, 4, 4}, // episode
	{0, 8, 12, 5}, // scene
	{0, 1, 5, 6}, // take
	{0, 2, 6, 3}, // video 1
	{0, 10, 14, 2}, // audio 1
	{0, 3, 7, 1}, // video 2
	{0, 11, 15, 0}, // audio 2
};
#define BUTTON_SECONDS_UP 9 /**< which of the 16 buttons is to increment the seconds */
#define BUTTON_SECONDS_DOWN 13 /**< which of the 16 buttons is to decrement the seconds */

// buttons to adjust numbers and seconds (mutliplexed in a 4x4 matrix)
#define BUTTONS_DRIVE_PORT A /**< port used to drive the buttons rows */
#define BUTTONS_DRIVE_PIN0 0 /**< pin used to drive buttons row 0 */
#define BUTTONS_DRIVE_PIN1 1 /**< pin used to drive buttons row 1 */
#define BUTTONS_DRIVE_PIN2 2 /**< pin used to drive buttons row 2 */
#define BUTTONS_DRIVE_PIN3 3 /**< pin used to drive buttons row 3 */
#define BUTTONS_READ_PORT A /**< port used to read the buttons columns */
#define BUTTONS_READ_PIN0 4 /**< pin used to read buttons column 0 */
#define BUTTONS_READ_PIN1 5 /**< pin used to read buttons column 1 */
#define BUTTONS_READ_PIN2 6 /**< pin used to read buttons column 2 */
#define BUTTONS_READ_PIN3 15 /**< pin used to read buttons column 3 */

#define CLAP_BUTTON_PORT B /**< port for button to detect clap action */
#define CLAP_BUTTON_PIN 14 /**< port for button to detect clap action */

/** Morse code variables, to buzz/beep scene and take */
#define MORSE_DOT 1 /**< unit Morse code duration in frames */
uint8_t morse[2*4*5*2] = {0}; /**< to encode 2 4-digit numbers (scene and take) into Morse code (5 sign+space) */
uint8_t morse_i = 0; /**< index in Morse array */
int16_t morse_delay = -1; /**< number of frames before beeping (-1=no need to beep yet, -2=beeping completed) */

/** user input command */
static char command[32] = {0};
/** user input command index */
uint8_t command_i = 0;

size_t putc(char c)
{
	size_t length = 0; // number of characters printed
	static char newline = 0; // to remember on which character we sent the newline
	if (0==c) {
		length = 0; // don't print string termination character
	} else if ('\r' == c || '\n' == c) { // send CR+LF newline for most carriage return and line feed combination
		if (0==newline || c==newline) { // send newline only if not already send (and only once on \r\n or \n\r)
				usart_putchar_nonblocking('\r'); // send CR over USART
				cdcacm_putchar('\r'); // send CR over USB
				usart_putchar_nonblocking('\n'); // send LF over USART
				cdcacm_putchar('\n'); // send LF over USB
				length += 2; // remember we printed 2 characters
				newline = c; // remember on which character we sent the newline
		} else {
			length = 0; // the \r or \n of \n\r or \r\n has already been printed
		}
	} else {
		usart_putchar_nonblocking(c); // send byte over USART
		cdcacm_putchar(c); // send byte over USB
		newline = 0; // clear new line
		length++; // remember we printed 1 character
	}
	return length; // return number of characters printed	
}

/** process user command
 *  @param[in] str user command string (\0 ended)
 */
static void process_command(char* str)
{
	// split command
	const char* delimiter = " ";
	char* word = strtok(str,delimiter);
	if (!word) {
		goto error;
	}
	// parse command
	if (0==strcmp(word,"help")) {
		printf("available commands:\n");
		printf("led [on|off|toggle]\n");
		printf("time [HH:MM:SS]\n");
		printf("date [YYYY-MM-DD]\n");
	} else if (0==strcmp(word,"led")) {
		word = strtok(NULL,delimiter);
		if (!word) {	
			goto error;
		} else if (0==strcmp(word,"on")) {
			led_on(); // switch LED on
			printf("LED switched on\n"); // notify user		
		} else if (0==strcmp(word,"off")) {
			led_off(); // switch LED off
			printf("LED switched off\n"); // notify user
		} else if (0==strcmp(word,"toggle")) {
			led_toggle(); // toggle LED
			printf("LED toggled\n"); // notify user
		} else {
			goto error;
		}
	} else if (0==strcmp(word,"time")) {
		word = strtok(NULL,delimiter);
		if (!word) {
			printf("current time: %02u:%02u:%02u\n", rtc_ds1307_read_hours(), rtc_ds1307_read_minutes(), rtc_ds1307_read_seconds()); // get and print time from external RTC
		} else if (strlen(word)!=8 || word[0]<'0' || word[0]>'2' || word[1]<'0' || word[1]>'9' || word[3]<'0' || word[3]>'5' || word[4]<'0' || word[4]>'9' || word[6]<'0' || word[6]>'5' || word[7]<'0' || word[7]>'9') { // time format is incorrect
				goto error;
		} else {
			if (!rtc_ds1307_write_hours((word[0]-'0')*10+(word[1]-'0')*1)) {
				printf("setting hours failed\n");
			} else if (!rtc_ds1307_write_minutes((word[3]-'0')*10+(word[4]-'0')*1)) {
				printf("setting minutes failed\n");
			} else if (!rtc_ds1307_write_seconds((word[6]-'0')*10+(word[7]-'0')*1)) {
				printf("setting seconds failed\n");
			} else {
				rtc_ds1307_oscillator_enable(); // be sure the oscillation is enabled
				printf("time set\n");
			}
		}
	} else if (0==strcmp(word,"date")) {
		word = strtok(NULL,delimiter);
		if (!word) {
			printf("current date: 20%02u-%02u-%02u\n", rtc_ds1307_read_year(), rtc_ds1307_read_month(), rtc_ds1307_read_date());
		} else if (strlen(word)!=10 || word[0]!='2' || word[1]!='0' || word[2]<'0' || word[2]>'9' || word[3]<'0' || word[3]>'9' || word[5]<'0' || word[5]>'1' || word[6]<'0' || word[6]>'9' || word[8]<'0' || word[8]>'3' || word[9]<'0' || word[9]>'9') {
				goto error;
		} else {
			if (!rtc_ds1307_write_year((word[2]-'0')*10+(word[3]-'0')*1)) {
				printf("setting year failed\n");
			} else if (!rtc_ds1307_write_month((word[5]-'0')*10+(word[6]-'0')*1)) {
				printf("setting month failed\n");
			} else if (!rtc_ds1307_write_date((word[8]-'0')*10+(word[9]-'0')*1)) {
				printf("setting day failed\n");
			} else {
				printf("date set\n");
			}
		}
	} else {
		goto error;
	}

	return; // command successfully processed
error:
	printf("command not recognized. enter help to list commands\n");
	return;
}

/** select clock output for TM1637 display using the analog multiplexer
 *  @param[in] output clock output channel
 */
static void mux_select(uint8_t output)
{
	if (output>7) { // multiplexer is only controlling 8 outputs
		return;
	}
	gpio_clear(GPIO(MUX_EN_PORT), GPIO(MUX_EN_PIN)); // enable multiplexer
	switch (output) {
		case 0: // output on channel C0
			gpio_clear(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_clear(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_clear(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		case 1: // output on channel C1
			gpio_set(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_clear(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_clear(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		case 2: // output on channel C2
			gpio_clear(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_set(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_clear(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		case 3: // output on channel C3
			gpio_set(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_set(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_clear(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		case 4: // output on channel C4
			gpio_clear(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_clear(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_set(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		case 5: // output on channel C5
			gpio_set(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_clear(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_set(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		case 6: // output on channel C6
			gpio_clear(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_set(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_set(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		case 7: // output on channel C7
			gpio_set(GPIO(MUX_S0_PORT), GPIO(MUX_S0_PIN));
			gpio_set(GPIO(MUX_S1_PORT), GPIO(MUX_S1_PIN));
			gpio_set(GPIO(MUX_S2_PORT), GPIO(MUX_S2_PIN));
			break;
		default:
			break;
	}
}

/** encode scene and take into Morse code (in morse variable) */
static void encode_morse(void)
{
	//  numbers (scene and take) to encode in Morse code
	uint16_t morse_numbers[] = {numbers[1].number, numbers[2].number};
	printf("morsing scene: %u, take: %u\n", morse_numbers[0], morse_numbers[1]);
	for (uint8_t morse_number=0; morse_number<LENGTH(morse_numbers); morse_number++) {
		bool not_zero = false; // has the first non-zero digit been found
		if (0xffff==morse_numbers[morse_number]) { // number disabled, don't beep
			for (uint8_t i=morse_number*4*2*5; i<(morse_number+1)*4*2*5; i++) {
				morse[i] = 0; // no beeping
			}
		} else { // encode number in Morse code
			for (uint8_t digit=0; digit<4; digit++) { // encode each digit
				uint16_t number = morse_numbers[morse_number]; // digit to encode
				// get only the digit from the number
				for (uint8_t divide=digit; divide<3; divide++) {
					number /= 10;
				}
				number %= 10;
				// remember when we found the first non-zero digit
				if (number!=0 || digit==3) {
					not_zero = true;
				}
				// encode digit in Morse code (1=short, 3=long)
				switch (number) {
					case 1: // Morse code for 1: short, long, long, long, long
						morse[(morse_number*4+digit)*2*5+0] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+2] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+4] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+6] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+8] = 3*MORSE_DOT; // long
						break;
					case 2: // Morse code for 2: short, short, long, long, long
						morse[(morse_number*4+digit)*2*5+0] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+2] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+4] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+6] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+8] = 3*MORSE_DOT; // long
						break;
					case 3: // Morse code for 3: short, short, short, long, long
						morse[(morse_number*4+digit)*2*5+0] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+2] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+4] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+6] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+8] = 3*MORSE_DOT; // long
						break;
					case 4: // Morse code for 4: short, short, short, short, long
						morse[(morse_number*4+digit)*2*5+0] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+2] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+4] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+6] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+8] = 3*MORSE_DOT; // long
						break;
					case 5: // Morse code for 5: short, short, short, short, short
						morse[(morse_number*4+digit)*2*5+0] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+2] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+4] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+6] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+8] = 1*MORSE_DOT; // short
						break;
					case 6: // Morse code for 6: long, short, short, short, short
						morse[(morse_number*4+digit)*2*5+0] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+2] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+4] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+6] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+8] = 1*MORSE_DOT; // short
						break;
					case 7: // Morse code for 7: long, long, short, short, short
						morse[(morse_number*4+digit)*2*5+0] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+2] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+4] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+6] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+8] = 1*MORSE_DOT; // short
						break;
					case 8: // Morse code for 8: long, long, long, short, short
						morse[(morse_number*4+digit)*2*5+0] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+2] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+4] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+6] = 1*MORSE_DOT; // short
						morse[(morse_number*4+digit)*2*5+8] = 1*MORSE_DOT; // short
						break;
					case 9: // Morse code for 9: long, long, long, long, short
						morse[(morse_number*4+digit)*2*5+0] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+2] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+4] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+6] = 3*MORSE_DOT; // long
						morse[(morse_number*4+digit)*2*5+8] = 1*MORSE_DOT; // short
						break;
					case 0: // Morse code for 0: long, long, long, long, long
						if (not_zero) { // encode zero if they are not leading
							morse[(morse_number*4+digit)*2*5+0] = 3*MORSE_DOT; // long
							morse[(morse_number*4+digit)*2*5+2] = 3*MORSE_DOT; // long
							morse[(morse_number*4+digit)*2*5+4] = 3*MORSE_DOT; // long
							morse[(morse_number*4+digit)*2*5+6] = 3*MORSE_DOT; // long
							morse[(morse_number*4+digit)*2*5+8] = 3*MORSE_DOT; // long
						} else { // don't encode the first digits if they are zero
							morse[(morse_number*4+digit)*2*5+0] = 0; // no beeping
							morse[(morse_number*4+digit)*2*5+2] = 0; // no beeping
							morse[(morse_number*4+digit)*2*5+4] = 0; // no beeping
							morse[(morse_number*4+digit)*2*5+6] = 0; // no beeping
							morse[(morse_number*4+digit)*2*5+8] = 0; // no beeping
						}
						break;
				}
				// fill the spaces between the dots
				for (uint8_t space=0; space<5; space++) {
					if (0==morse[(morse_number*4+digit)*2*5+space*2]) { // don't add space if there is no Morse code
						morse[(morse_number*4+digit)*2*5+space*2+1] = 0; // no space
					} else { // add space between Morse codes
						morse[(morse_number*4+digit)*2*5+space*2+1] = 1*MORSE_DOT; // add space
					}
				}
			}
		}
	}
	morse[4*2*5+1] = 3*MORSE_DOT; // use longer silence between the two numbers
	morse_i = 0; // reset Morse index
}

/** program entry point
 *  this is the firmware function started by the micro-controller
 */
void main(void);
void main(void)
{	
	rcc_clock_setup_in_hse_8mhz_out_72mhz(); // use 8 MHz high speed external clock to generate 72 MHz internal clock

	// keep power on a soon as possible
	rcc_periph_clock_enable(RCC_GPIO(POWER_SWITCH_PORT)); // enable clock for GPIO
	gpio_set_mode(GPIO(POWER_SWITCH_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(POWER_SWITCH_PIN)); // set as output to control power
	gpio_set(GPIO(POWER_SWITCH_PORT), GPIO(POWER_SWITCH_PIN)); // enable power by saturating nMOS controlling power
	rcc_periph_clock_enable(RCC_GPIO(POWER_BUTTON_PORT)); // enable clock for GPIO domain
	gpio_set_mode(GPIO(POWER_BUTTON_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO(POWER_BUTTON_PIN)); // set pin as input to detect power switching activity
	rcc_periph_clock_enable(RCC_AFIO); // enable alternate function clock for external interrupt
	exti_select_source(EXTI(POWER_BUTTON_PIN), GPIO(POWER_BUTTON_PORT)); // mask external interrupt of this pin only for this port
	exti_set_trigger(EXTI(POWER_BUTTON_PIN), EXTI_TRIGGER_BOTH); // trigger on any activity of the power switch
	exti_enable_request(EXTI(POWER_BUTTON_PIN)); // enable external interrupt
	nvic_enable_irq(NVIC_EXTI_IRQ(POWER_BUTTON_PIN)); // enable interrupt

#if DEBUG
	// enable functionalities for easier debug
	DBGMCU_CR |= DBGMCU_CR_IWDG_STOP; // stop independent watchdog counter when code is halted
	DBGMCU_CR |= DBGMCU_CR_WWDG_STOP; // stop window watchdog counter when code is halted
	DBGMCU_CR |= DBGMCU_CR_STANDBY; // allow debug also in standby mode (keep digital part and clock powered)
	DBGMCU_CR |= DBGMCU_CR_STOP; // allow debug also in stop mode (keep clock powered)
	DBGMCU_CR |= DBGMCU_CR_SLEEP; // allow debug also in sleep mode (keep clock powered)
#else
	// setup watchdog to reset in case we get stuck (i.e. when an error occurred)
	iwdg_set_period_ms(WATCHDOG_PERIOD); // set independent watchdog period
	iwdg_start(); // start independent watchdog
#endif

	board_setup(); // setup board
    usart_setup(); // setup USART for user communication
	cdcacm_setup(); // setup USB ACM (serial) for user communication
	gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON,0); // disable JTAG (but leave SWD on) since we need most of the GPIOs
	printf("\nwelcome to the CuVoodoo clapperboard\n"); // print welcome message

#if !(DEBUG)
	// show watchdog information
	printf("watchdog set to (%2u.%2us)\n",WATCHDOG_PERIOD/1000, (WATCHDOG_PERIOD/10)%100);
	if (FLASH_OBR&FLASH_OBR_OPTERR) {
		printf("option bytes not set in flash: software wachtdog used (not started at reset)\n");
	} else if (FLASH_OBR&FLASH_OBR_WDG_SW) {
		printf("software wachtdog used (not started at reset)\n");
	} else {
		printf("hardware wachtdog used (started at reset)\n");
	}
#endif

	// setup external RTC
	printf("setup external RTC: ");
	rtc_ds1307_setup(); // setup external RTC module
	// enable square wave output and configure input interrupt to count seconds
	rtc_ds1307_write_square_wave(1); // enable 1 Hz square wave output to sync on seconds
	rcc_periph_clock_enable(RCC_GPIO(SQUARE_WAVE_PORT)); // enable clock for GPIO
	gpio_set_mode(GPIO(SQUARE_WAVE_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO(SQUARE_WAVE_PIN)); // set pin to input
	rcc_periph_clock_enable(RCC_AFIO); // enable alternate function clock for external interrupt
	exti_select_source(EXTI(SQUARE_WAVE_PIN), GPIO(SQUARE_WAVE_PORT)); // mask external interrupt of this pin only for this port
	exti_set_trigger(EXTI(SQUARE_WAVE_PIN), EXTI_TRIGGER_FALLING); // trigger on falling edge of square wave (this is also when the RTC register are updated)
	exti_enable_request(EXTI(SQUARE_WAVE_PIN)); // enable external interrupt
	nvic_enable_irq(NVIC_EXTI_IRQ(SQUARE_WAVE_PIN)); // enable interrupt
	printf("OK\n");

	// verify if external RTC is running
	if (rtc_ds1307_oscillator_disabled()) {
		printf("/!\\ RTC oscillator is disabled: the battery may be empty\n");
		rtc_ds1307_oscillator_enable(); // enable oscillator again
	}
	// get date
	uint8_t* rtc_ds1307_time = rtc_ds1307_read_time(); // get time/date from external RTC
	if (rtc_ds1307_time==NULL) {
		printf("could not get time from DS1307\n");
	} else {
		rtc_seconds = rtc_ds1307_time[0]; // remember seconds of minute
		printf("current date: 20%02u-%02u-%02u %02u:%02u:%02u\n", rtc_ds1307_time[6], rtc_ds1307_time[5], rtc_ds1307_time[4], rtc_ds1307_time[2], rtc_ds1307_time[1], rtc_ds1307_time[0]);
	}

	// setup analog multiplexer for TM1637 clock signal (mutliplex the clock signal we output between the 7 TM1637 displays)
	printf("setup multiplexer: ");
	rcc_periph_clock_enable(RCC_GPIO(MUX_EN_PORT)); // enable clock for GPIO port domain
	gpio_set_mode(GPIO(MUX_EN_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(MUX_EN_PIN)); // to enable/disable the multiplexer
	gpio_set(GPIO(MUX_EN_PORT), GPIO(MUX_EN_PIN)); // disable multiplexer
	rcc_periph_clock_enable(RCC_GPIO(MUX_S0_PORT)); // enable clock for GPIO port domain
	gpio_set_mode(GPIO(MUX_S0_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(MUX_S0_PIN)); // to select output channel
	rcc_periph_clock_enable(RCC_GPIO(MUX_S1_PORT)); // enable clock for GPIO port domain
	gpio_set_mode(GPIO(MUX_S1_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(MUX_S1_PIN)); // to select output channel
	rcc_periph_clock_enable(RCC_GPIO(MUX_S2_PORT)); // enable clock for GPIO port domain
	gpio_set_mode(GPIO(MUX_S2_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(MUX_S2_PIN)); // to select output channel
	printf("OK\n");

	// setup TM1637 4-digit 7-segment displays to display the numbers
	printf("setup 7-segment displays: ");
	led_tm1637_setup(); // setup TM1637
	for (uint8_t tm1637=0; tm1637<LENGTH(numbers); tm1637++) {
		mux_select(tm1637);
		if (!led_tm1637_time(88,88)) { // test TM1637 display
			printf("could not test TM1637 %u\n", tm1637);
		}
	}
	// setup MAX7219 8-digit 7-segment displays to display date and time
	led_max7219_setup(2); // setup MAX7219
	led_max7219_intensity(15,8,0xff); // set brightness max and enable all digits
	led_max7219_test(true,0xff); // test all MAX7219 displays
	for (volatile uint32_t i=0; i<10000000; i++); // wait a bit to have the user check the displays
	// switch displays back off
	for (uint8_t tm1637=0; tm1637<LENGTH(numbers); tm1637++) {
		mux_select(tm1637);
		if (!led_tm1637_off()) { // switch off display
			printf("could not switch off TM1637 %u\n", tm1637);
		}
	}
	led_max7219_test(false,0xff); // go back in normal operation
	led_max7219_off(0xff); // switch displays off
	printf("OK\n");

	// get and display numbers on TM1637
	for (uint8_t number=0; number<LENGTH(numbers); number++) {
		uint8_t bytes[2] = {0}; // bytes for the numbers
		if (!rtc_ds1307_read_rom(number*LENGTH(bytes), bytes, LENGTH(bytes))) { // get number from EEPROM
			printf("could not get number from EEPROM\n");
		}
		numbers[number].number = (bytes[0]<<8)+bytes[1]; // save number
		// display number
		mux_select(numbers[number].display);
		printf("number %u: ", number);
		if (0xffff==numbers[number].number) {
			printf("off\n");
			led_tm1637_off();
		} else {
			printf("%u\n", numbers[number].number);
			led_tm1637_number(numbers[number].number);
		}
	}

	// display date and time on MAX7219 displays
	led_max7219_number(20000000+rtc_ds1307_time[6]*10000+rtc_ds1307_time[5]*100+rtc_ds1307_time[4], 0x14, 0); // display date on 1st display
	led_max7219_number(rtc_ds1307_time[2]*1000000+rtc_ds1307_time[1]*10000+rtc_ds1307_time[0]*100, 0x54, 1); // display time on 2nd display
	led_max7219_on(0xff); // switch displays on

	// setup timer to count frames
	printf("setup frame timer: ");
	rcc_periph_clock_enable(RCC_TIM(FRAME_TIMER)); // enable clock for timer domain
	timer_reset(TIM(FRAME_TIMER)); // reset timer state
	timer_set_mode(TIM(FRAME_TIMER), TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP); // set timer mode, use undivided timer clock, edge alignment (simple count), and count up
	timer_set_prescaler(TIM(FRAME_TIMER), (rcc_ahb_frequency/0xffff+1)-1); // set the prescaler to so count up to one second
	timer_set_period(TIM(FRAME_TIMER), 0xffff/FRAME_RATE); // overflow at the end of every rate
	timer_update_on_overflow(TIM(FRAME_TIMER)); // only use counter overflow as UEV source (use overflow as timeout)
	timer_clear_flag(TIM(FRAME_TIMER), TIM_SR_UIF); // clear flag
	timer_enable_irq(TIM(FRAME_TIMER), TIM_DIER_UIE); // enable update interrupt for timer
#if FRAME_TIMER==1
	nvic_enable_irq(NVIC_TIM1_UP_IRQ); // catch interrupt in service routine
#else
	nvic_enable_irq(NVIC_TIM_IRQ(FRAME_TIMER)); // catch interrupt in service routine
#endif
	timer_set_counter(TIM(FRAME_TIMER),0); // restart timer
	timer_enable_counter(TIM(FRAME_TIMER)); // enable timer to start counting frames
	printf("OK\n");

	// setup PWM for piezo-buzzer
	printf("setup piezo-buzzer PWM timer: ");
	rcc_periph_clock_enable(RCC_GPIO(BUZZER_1_PORT)); // enable clock for GPIO port domain
	rcc_periph_clock_enable(RCC_AFIO); // enable clock for alternate function (PWM)
	gpio_primary_remap(AFIO_MAPR_SWJ_MASK, AFIO_MAPR_TIM1_REMAP_PARTIAL_REMAP); // remap TIM1_CH1N to PA7 instead of PB13
	gpio_set_mode(GPIO(BUZZER_1_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO(BUZZER_1_PIN)); // set pin as output to have a PWM to driver piezo buzzer
	gpio_clear(GPIO(BUZZER_1_PORT), GPIO(BUZZER_1_PIN)); // ensure it's not beeping
	rcc_periph_clock_enable(RCC_GPIO(BUZZER_2_PORT)); // enable clock for GPIO peripheral
    gpio_set_mode(GPIO(BUZZER_2_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO(BUZZER_2_PIN)); // set pin as output to have a PWM to driver piezo buzzer
	gpio_clear(GPIO(BUZZER_2_PORT), GPIO(BUZZER_2_PIN)); // ensure it's not beeping
	rcc_periph_clock_enable(RCC_TIM(BUZZER_TIMER)); // enable clock for timer peripheral
	timer_reset(TIM(BUZZER_TIMER)); // reset timer state
	timer_set_mode(TIM(BUZZER_TIMER), TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP); // set timer mode, use undivided timer clock, edge alignment (simple count), and count up
	timer_set_prescaler(TIM(BUZZER_TIMER), 0); // no prescaler to keep most precise timer (72MHz/2^16=1099<4kHz)
	timer_set_period(TIM(BUZZER_TIMER), rcc_ahb_frequency/4000-1); // set the PWM frequency to 4kHz for piezo-buzzer
	timer_set_oc_value(TIM(BUZZER_TIMER), TIM_OC1, rcc_ahb_frequency/4000/2-1); // duty cycle to 50% (also applies to TIM_OC1N)
	// no preload is used, although the reference manual says to enable it
	timer_set_oc_mode(TIM(BUZZER_TIMER), TIM_OC1, TIM_OCM_PWM1); // set timer to generate PWM (also applies to TIM_OC1N)
	timer_enable_oc_output(TIM(BUZZER_TIMER), TIM_OC1); // enable output to generate PWM
	timer_enable_oc_output(TIM(BUZZER_TIMER), TIM_OC1N); // enable output to generate PWM (complementary to be louder)
	timer_enable_break_main_output(TIM(BUZZER_TIMER)); // enable master output
	timer_generate_event(TIM(BUZZER_TIMER), TIM_EGR_UG); // generate update event to reload registers and reset counter
	// switch buzzer off by removing the output from the PWM
	gpio_clear(GPIO(BUZZER_1_PORT), GPIO(BUZZER_1_PIN));
	gpio_set_mode(GPIO(BUZZER_1_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUZZER_1_PIN));
	gpio_clear(GPIO(BUZZER_2_PORT), GPIO(BUZZER_2_PIN));
	gpio_set_mode(GPIO(BUZZER_2_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUZZER_2_PIN));
	printf("OK\n");

	// setup GPIO for reading buttons
	printf("setup display buttons: ");
	rcc_periph_clock_enable(RCC_GPIO(BUTTONS_DRIVE_PORT)); // enable clock for GPIO port domain
	// no need to configure the driving line modes since this will be done when they need to be driven individually
	rcc_periph_clock_enable(RCC_GPIO(BUTTONS_READ_PORT)); // enable clock for GPIO port domain
	gpio_set_mode(GPIO(BUTTONS_READ_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO(BUTTONS_READ_PIN0)|GPIO(BUTTONS_READ_PIN1)|GPIO(BUTTONS_READ_PIN2)|GPIO(BUTTONS_READ_PIN3)); // set read lines as input (external pull down resistors are required)
	gpio_primary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON, 0); // enable PA15 (JTDI per default)
	uint16_t buttons = 0; // to save button state
	printf("OK\n");

	printf("setup clap button: ");
	rcc_periph_clock_enable(RCC_GPIO(CLAP_BUTTON_PORT)); // enable clock for GPIO port domain
	gpio_set_mode(GPIO(CLAP_BUTTON_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO(CLAP_BUTTON_PIN)); // set button pin to input
	gpio_set(GPIO(CLAP_BUTTON_PORT), GPIO(CLAP_BUTTON_PIN)); // pull up
	rcc_periph_clock_enable(RCC_AFIO); // enable alternate function clock for external interrupt
	exti_select_source(EXTI(CLAP_BUTTON_PIN), GPIO(CLAP_BUTTON_PORT)); // mask external interrupt of this pin only for this port
	exti_set_trigger(EXTI(CLAP_BUTTON_PIN), EXTI_TRIGGER_FALLING); // trigger on falling edge (we are pulling up on one side, and ground is on the other side)
	exti_enable_request(EXTI(CLAP_BUTTON_PIN)); // enable external interrupt
	nvic_enable_irq(NVIC_EXTI_IRQ(CLAP_BUTTON_PIN)); // enable interrupt
	printf("OK\n");

	// main loop
	printf("command input: ready\n");
	bool action = false; // if an action has been performed don't go to sleep
	char c = '\0'; // to store received character
	bool char_flag = false; // a new character has been received
	while (true) { // main loop
		iwdg_reset(); // kick the dog
		while (usart_received) { // data received over UART
			action = true; // action has been performed
			led_toggle(); // toggle LED to show activity
			c = usart_getchar(); // store receive character
			standby_timer = 0; // restart standby timer
			char_flag = true; // notify character has been received
		}
		while (cdcacm_received) { // data received over USB
			action = true; // action has been performed
			led_toggle(); // toggle LED to show activity
			c = cdcacm_getchar(); // store receive character
			standby_timer = 0; // restart standby timer
			char_flag = true; // notify character has been received
		}
		while (char_flag) { // user data received
			char_flag = false; // reset flag
			action = true; // action has been performed
			putc(c); // echo receive character
			if (c=='\r' || c=='\n') { // end of command received
				if (command_i>0) { // there is a command to process
					command[command_i] = 0; // end string
					command_i = 0; // prepare for next command
					process_command(command); // process user command
				}
			} else { // save user command input
				command[command_i] = c; // save command input
				if (command_i<LENGTH(command)-2) { // verify if there is place to save next character
					command_i++; // save next character
				}
			}
		}
		while (frame_flag) { // a frame has passed
			frame_flag = false; // reset flag
			action = true; // action has been performed

			// display time and frame number
			char time[] = "00000000"; // time to display
			time[0] += (rtc_ds1307_time[2]/10)%10; // display hours
			time[1] += (rtc_ds1307_time[2])%10; // display hours
			time[1] |= 0x80; // dot for minutes on display
			time[2] += (rtc_ds1307_time[1]/10)%10; // display minutes
			time[3] += (rtc_ds1307_time[1])%10; // display minutes
			if (0==(rtc_seconds%2)) { // even seconds
				time[3] |= 0x80; // add dot for seconds
			}
			time[4] += (rtc_seconds/10)%10; // display seconds
			time[5] += (rtc_seconds)%10; // display seconds
			if (0==(frame_count%2)) { // even frames
				time[5] |= 0x80; // add dot for frame
			}
			time[6] += (frame_count/10)%10; // display frame
			time[7] += (frame_count)%10; // display frame
			led_max7219_text(time, 1); // display frame time on 2nd display

			// Morse the scene and take numbers over the buzzer
			if (morse_delay>0) { // not ready to Morse
				morse_delay--; // still wait before Morsing
			} else if (0==morse_delay) { // ready to Morse
				while (morse_i<LENGTH(morse)) { // until every code has been Morsed
					if (morse[morse_i]) { // skip empty codes
						if (morse_i%2) { // switch buzzer on for odd code
							timer_disable_counter(TIM(BUZZER_TIMER)); // stop buzzing
							// switch buzzer off by removing the output from the PWM
							gpio_clear(GPIO(BUZZER_1_PORT), GPIO(BUZZER_1_PIN));
							gpio_set_mode(GPIO(BUZZER_1_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUZZER_1_PIN));
							gpio_clear(GPIO(BUZZER_2_PORT), GPIO(BUZZER_2_PIN));
							gpio_set_mode(GPIO(BUZZER_2_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUZZER_2_PIN));
						} else {
							// allow PWM to control the output
							gpio_set_mode(GPIO(BUZZER_1_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO(BUZZER_1_PIN));
							gpio_set_mode(GPIO(BUZZER_2_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO(BUZZER_2_PIN));
							timer_enable_counter(TIM(BUZZER_TIMER)); // start buzzing
						}
						morse[morse_i]--; // decrement remaining buzzing time
						break; // buzz for one frame
					} else {
						morse_i++; // got to next code
					}
				}
				if (morse_i>=LENGTH(morse)) { // all codes done
					morse_delay = -2; // Morse completed
					// switch buzzer off by removing the output from the PWM
					gpio_clear(GPIO(BUZZER_1_PORT), GPIO(BUZZER_1_PIN));
					gpio_set_mode(GPIO(BUZZER_1_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUZZER_1_PIN));
					gpio_clear(GPIO(BUZZER_2_PORT), GPIO(BUZZER_2_PIN));
					gpio_set_mode(GPIO(BUZZER_2_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUZZER_2_PIN));
				}
			}

			// read button inputs (drive and read each row since they are multiplexed)
			uint16_t buttons_new = 0; // to save new button states
			// drive row 0
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUTTONS_DRIVE_PIN0));
			gpio_set(GPIO(BUTTONS_DRIVE_PORT), GPIO(BUTTONS_DRIVE_PIN0));
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO(BUTTONS_DRIVE_PIN1)|GPIO(BUTTONS_DRIVE_PIN2)|GPIO(BUTTONS_DRIVE_PIN3));
			// read all columns for this row
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN0)) ? 1 : 0)<<0);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN1)) ? 1 : 0)<<1);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN2)) ? 1 : 0)<<2);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN3)) ? 1 : 0)<<3);
			// drive row 1
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUTTONS_DRIVE_PIN1));
			gpio_set(GPIO(BUTTONS_DRIVE_PORT), GPIO(BUTTONS_DRIVE_PIN1));
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO(BUTTONS_DRIVE_PIN0)|GPIO(BUTTONS_DRIVE_PIN2)|GPIO(BUTTONS_DRIVE_PIN3));
			// read all columns for this row
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN0)) ? 1 : 0)<<4);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN1)) ? 1 : 0)<<5);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN2)) ? 1 : 0)<<6);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN3)) ? 1 : 0)<<7);
			// drive row 2
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUTTONS_DRIVE_PIN2));
			gpio_set(GPIO(BUTTONS_DRIVE_PORT), GPIO(BUTTONS_DRIVE_PIN2));
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO(BUTTONS_DRIVE_PIN0)|GPIO(BUTTONS_DRIVE_PIN1)|GPIO(BUTTONS_DRIVE_PIN3));
			// read all columns for this row
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN0)) ? 1 : 0)<<8);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN1)) ? 1 : 0)<<9);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN2)) ? 1 : 0)<<10);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN3)) ? 1 : 0)<<11);
			// drive row 3
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(BUTTONS_DRIVE_PIN3));
			gpio_set(GPIO(BUTTONS_DRIVE_PORT), GPIO(BUTTONS_DRIVE_PIN3));
			gpio_set_mode(GPIO(BUTTONS_DRIVE_PORT), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO(BUTTONS_DRIVE_PIN0)|GPIO(BUTTONS_DRIVE_PIN1)|GPIO(BUTTONS_DRIVE_PIN2));
			// read all columns for this row
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN0)) ? 1 : 0)<<12);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN1)) ? 1 : 0)<<13);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN2)) ? 1 : 0)<<14);
			buttons_new |= ((gpio_get(GPIO(BUTTONS_READ_PORT), GPIO(BUTTONS_READ_PIN3)) ? 1 : 0)<<15);
			uint16_t buttons_diff = buttons^buttons_new; // get difference
			buttons = buttons_new; // save new state
			if (buttons_diff) { // only do something if there is a change
				printf("button pressed: %016b\n", buttons);
				for (uint8_t number=0; number<LENGTH(numbers); number++) { // check buttons for every number
					if (buttons_diff&(1<<numbers[number].up) || buttons_diff&(1<<numbers[number].down)) { // a button for the number has been pressed
						if (buttons&(1<<numbers[number].up) && buttons&(1<<numbers[number].down)) { // both buttons are pressed
							numbers[number].number = 0xffff; // disable number
						} else if (buttons&(1<<numbers[number].up) && buttons_diff&(1<<numbers[number].up)) { // only up button has been pressed
							// increment number
							if (0xffff==numbers[number].number) {
								numbers[number].number = 0;
							} else {
								numbers[number].number = (numbers[number].number+1)%10000;
							}
						} else if (buttons&(1<<numbers[number].down) && buttons_diff&(1<<numbers[number].down)) { // only down button has been pressed
							// decrement number
							if (0xffff==numbers[number].number) {
								numbers[number].number = 9999;
							} else if (0==numbers[number].number) {
								numbers[number].number = 0xffff;
							} else {
								numbers[number].number -= 1;
							}
						}
						// display numbers on TM1637
						mux_select(numbers[number].display);
						if (0xffff==numbers[number].number) {
							led_tm1637_off();
						} else {
							led_tm1637_number(numbers[number].number);
						}
						// save number
						uint8_t bytes[2] = {numbers[number].number>>8, numbers[number].number};
						if (!rtc_ds1307_write_rom(number*LENGTH(bytes), bytes, LENGTH(bytes))) {
							printf("could not set number on EEPROM\n");
						}
					}
				}
				if ((buttons_diff&(1<<BUTTON_SECONDS_UP) && buttons&(1<<BUTTON_SECONDS_UP)) || (buttons_diff&(1<<BUTTON_SECONDS_DOWN)  && buttons&(1<<BUTTON_SECONDS_DOWN))) { // buttons for time pressed
					rtc_ds1307_time = rtc_ds1307_read_time(); // get time/date from external RTC
					if (rtc_ds1307_time==NULL) {
						printf("could not get time from DS1307: resetting\n");
						rtc_ds1307_setup(); // resetting peripheral
					} else {
						// adjust time
						if (buttons_diff&(1<<BUTTON_SECONDS_UP) && buttons&(1<<BUTTON_SECONDS_UP)) { // up pressed
							// increment time by one second
							if (!(23==rtc_ds1307_time[2] && 59==rtc_ds1307_time[1] && 59==rtc_ds1307_time[0])) { // don't handle date changes since it's too complex
								rtc_ds1307_time[0] += 1;
								while (rtc_ds1307_time[0]>=60) {
									rtc_ds1307_time[0] -= 60;
									rtc_ds1307_time[1] += 1;
								}
								while (rtc_ds1307_time[1]>=60) {
									rtc_ds1307_time[1] -= 60;
									rtc_ds1307_time[2] += 1;
								}
								while (rtc_ds1307_time[2]>=24) {
									rtc_ds1307_time[2] -= 24;
								}
							}
						} else if (buttons_diff&(1<<BUTTON_SECONDS_DOWN) && buttons&(1<<BUTTON_SECONDS_DOWN)) { // down pressed
							// decrement time by one second
							if (!(0==rtc_ds1307_time[2] && 0==rtc_ds1307_time[1] && 0==rtc_ds1307_time[0])) { // don't handle date changes since it's too complex
								if (rtc_ds1307_time[0]>0) {
									rtc_ds1307_time[0] -= 1;
								} else {
									rtc_ds1307_time[0] = 59;
									if (rtc_ds1307_time[1]>0) {
										rtc_ds1307_time[1] -= 1;
									} else {
										rtc_ds1307_time[1] = 59;
										rtc_ds1307_time[2] -= 1; // it can't be 0
									}
								}
							}
						}
						rtc_seconds = rtc_ds1307_time[0]; // save seconds since this is only updated every minute
						if (!rtc_ds1307_write_time(rtc_ds1307_time[0], rtc_ds1307_time[1], rtc_ds1307_time[2], rtc_ds1307_time[3], rtc_ds1307_time[4], rtc_ds1307_time[5], rtc_ds1307_time[6])) { // save adjusted time to DS1307
							printf("could not set time on DS1307: resetting\n");
							rtc_ds1307_setup(); // resetting peripheral
						}
					}
				}
				standby_timer = 0; // restart standby timer
			}
			
		}
		while (rtc_tick_flag) { // the external RTC ticked
			rtc_tick_flag = false; // reset flag
			action = true; // action has been performed
			led_toggle(); // toggle LED (good to indicate if main function is stuck)
			if (standby_timer>=STANDBY_TIMEOUT) { // standby timeout complete
				// go into standby mode
				printf("shutting down\n");
				// increment and save take and video/audio numbers if there has been a clap
				if (-2==morse_delay) {
					morse_delay = -1; // prevent incrementing multiple times
					uint8_t bytes[LENGTH(numbers)*2] = {0}; // all numbers to write again in EEPROM
					for (uint8_t number=0; number<LENGTH(numbers); number++) { // increment numbers
						if (number!=0 && number!=1 && numbers[number].number!=0xffff) { // don't increment episode, scene, or disabled numbers
							numbers[number].number = (numbers[number].number+1)%10000;
							printf("incrementing %u to %u\n", number, numbers[number].number);
						}
						bytes[2*number+0] = numbers[number].number>>8; // set number to save
						bytes[2*number+1] = numbers[number].number; // set number to save
					}
					if (!rtc_ds1307_write_rom(0, bytes, LENGTH(bytes))) { // save numbers to EEPROM
						printf("could not set numbers on EEPROM\n");
					}
				}
				for (uint8_t tm1637=0; tm1637<7; tm1637++) { // switch off TM1637 displays
					mux_select(tm1637); // selecting TM1637 display
					led_tm1637_off(); // switch off TM1637 display
				}
				led_max7219_off(0xff); // switch off MAX7219 displays
				timer_disable_counter(TIM(BUZZER_TIMER)); // stop buzzing
				gpio_clear(GPIO(POWER_SWITCH_PORT), GPIO(POWER_SWITCH_PIN)); // switch power of by disconnecting from battery
				SCB_SCR |= SCB_SCR_SLEEPDEEP; // enable deep sleep
				pwr_set_standby_mode(); // go to deep sleep
				while (true); // we should be shut down at this point
			}
			if (rtc_seconds>=60) { // one minute passed
				rtc_ds1307_time = rtc_ds1307_read_time(); // get time/date from external RTC
				if (rtc_ds1307_time==NULL) {
					printf("could not get time from DS1307: resetting\n");
					rtc_ds1307_setup(); // resetting peripheral
				} else {
					rtc_seconds = rtc_ds1307_time[0]; // get actual number of seconds
					if (0==rtc_ds1307_time[1] && 0==rtc_ds1307_time[2]) { // new day arrived
						led_max7219_number(20000000+rtc_ds1307_time[6]*10000+rtc_ds1307_time[5]*100+rtc_ds1307_time[4], 0x14, 0); // display date on 1st display
					}
				}
			}
		}
		while (keep_alive_flag) { // power switch is detecting movement to keep clapperboard running
			action = true; // action has been performed
			keep_alive_flag = false; // clear flag
			standby_timer = 0; // restart standby timer
		}
		while (clap_flag) { // clap detected
			action = true; // action has been performed
			clap_flag = false; // clear flag
			if (morse_delay<0) { // if beeping is not already queued
				encode_morse(); // encode scene and take number into Morse code
				morse_delay = FRAME_RATE; // wait 1 second
			}
		}
		if (action) { // go to sleep if nothing had to be done, else recheck for activity
			action = false;
		} else {
			__WFI(); // go to sleep and wait for interrupt
		}
	} // main loop
}

/** RTC square wave input ISR to synchronize to seconds and count them */
void EXTI_ISR(SQUARE_WAVE_PIN)(void)
{
    exti_reset_request(EXTI(SQUARE_WAVE_PIN)); // reset interrupt
	frame_count = 0; // re-sync frame counter to second
	rtc_seconds++; // increment number of seconds passed
	if (standby_timer<STANDBY_TIMEOUT) { // timeout countdown not complete
		standby_timer++; // continue counting down
	}
    rtc_tick_flag = true; // let main know a second passed
}

/** frame timer ISR */
#if FRAME_TIMER==1
void tim1_up_isr(void)
#else
void TIM_ISR(FRAME_TIMER)(void)
#endif
{
	if (timer_get_flag(TIM(FRAME_TIMER), TIM_SR_UIF)) { // overflow update event happened
		timer_clear_flag(TIM(FRAME_TIMER), TIM_SR_UIF); // clear flag
		frame_count++; // increment frame counter since frame finished
		frame_flag = true; // let main know the counter changed
	} else { // no other interrupt should occur
		while (true); // unhandled exception: wait for the watchdog to bite
	}
}

/** power switch/keep alive activity */
void EXTI_ISR(POWER_BUTTON_PIN)(void)
{
	exti_reset_request(EXTI(POWER_BUTTON_PIN)); // reset interrupt
	keep_alive_flag = true; // perform button action
}

/** clap */
void EXTI_ISR(CLAP_BUTTON_PIN)(void)
{
    exti_reset_request(EXTI(CLAP_BUTTON_PIN)); // reset interrupt
	clap_flag = true; // perform clap action
}
