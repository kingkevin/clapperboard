project
=======

summary
-------

This is the firmware for a digital clapperboard.

technology
----------

Clapperboards are used to synchronize audio and video tracks by clapping two wooden pieces.
This is then visible on the video and the sharp sound is also present on the audio.
Using the scene and take number written on the clapperboard and announced at the beginning of the recording it is possible to know which sound track corresponds to which video track.

This clapper board improves this system with some features:

- use 7-segment displays to show number, instead of having to manually write and erase it on the board
  - episode, scene, and take 
  - two audio and video track numbers (number used in the file names or the recordings)
  - current date, time, and frame number
- automatically increment the take and recording numbers

The numbers on the board can be adjusted by hand using buttons.

board
=====

The current implementation uses a [core board](https://wiki.cuvoodoo.info/doku.php?id=stm32f1xx#core_board).
The underlying [template](https://git.cuvoodoo.info/stm32f1/about/) supports additional board.

peripherals
===========

- 1x 8-digit 7-segment display based on the MAX7219 to display the date
- 1x 8-digit 7-segment display based on the MAX7219 to display the time (hours, minutes, seconds) and frame number
- 7x 4-digit 7-segment display based on the TM1637 to display recording information
	- 1x to display the episode (or chapter)
	- 1x to display the scene
	- 1x to display the take
	- 2x to display the video file number
	- 2x to display the audio file number
- 1x 16-channel multiplexer to communicate with the 7-segments individually
- 8x2 button switches to adjust the recording numbers and seconds
- 1x button switch to detect clap
- 1x shake sensor to detect movement and switch on the clapper board
- 1x DS1307 RTC to keep track of the time (using a tiny RTC module)
- 1x I2C EEPROM to save numbers (also present on the tiny RTC module)
- 1x power switching board (to switch off power completely)
- 1x battery + protection, charge, and 5V step up circuit

connections
===========

Connect the peripherals the following way (peripheral signal; peripheral pin; micro-controller pin; micro-controller signal; comment):

- DS1307 RTC to get the date and EEPROM to save the numbers
	- VCC; 8; ; +5V;
	- GND; 4; ; GND;
	- SCL; 6; PB10; I2C2_SCL;
	- SDA; 5; PB11; I2C2_SDA;
	- SQW; 7; PBO; GPIO; to synchronize the seconds
- 2x MAX7219 in chain to show date and time
	- VCC; 19; ; +5V;
	- GND; 4; ; GND;
	- CLK; 13; PB13; SPI2_CLK;
	- DIN; 1; PB15; SPI2_MOSI;
	- LOAD; 12; PB12; SPI2_NSS; used as GPIO
- 7x TM1637 to show the recording numbers
	- VDD; 16; ; +5V;
	- GND; 1; ; GND;
	- DIO; 17; PB7; GPIO; display with no clock signal will ignore data activity
	- CLK; 18; 3-9; I0-I6; on analog multiplexer
- 1x CD74HC4067 16-channel multiplexer
	- VCC; 24; ; +5V;
	- GND; 12; ; GND;
	- common input; 1; PB6; GPIO; clock for TM1637
	- -E; 15; PB9; GPIO; to enable on low
	- S0; 10; PB3; GPIO; to select output
	- S1; 11; PB4; GPIO; to select output
	- S2; 14; PB5; GPIO; to select output
	- S3; 13; ; GND;
	- I0; 9; 18; CLK; TM1637 for audio 2 
	- I1; 8; 18; CLK; TM1637 for video 2 
	- I2; 7; 18; CLK; TM1637 for audio 1
	- I3; 6; 18; CLK; TM1637 for video 1
	- I4; 5; 18; CLK; TM1637 for episode 
	- I5; 4; 18; CLK; TM1637 for scene
	- I6; 3; 18; CLK; TM1637 for take
- 1x power board (custom), including SW-18015P shake switch between collector of PNP and emitter (with pull-down resistor and diode between shake switch and emitter)
	- GND; ; GND; step up voltage regulator;
	- IN; ; 5V; step up voltage regulator;
	- OUT; ; +5V; all other peripherals
	- base of PNP; SWITCH; PB8; to control power to all peripherals
	- not-collector connected pin of shake switch; ACTIVITY; PB1; to detect power-up activity
- 1x clap switch
	- pin 1; 1; ; GND;
	- pin 2; 2; PB14; ; to detect clapping action (add pull-up resistor to improve detection)
- 16x buttons, in a 4x4 matrix
	- 4x row 0 buttons; ; PA0; ; row 0
	- 4x row 1 buttons; ; PA1; ; row 1
	- 4x row 2 buttons; ; PA2; ; row 2
	- 4x row 3 buttons; ; PA3; ; row 3
	- 4x column 0 buttons; ; PA4; ; column 0, with a pull-down resistor
	- 4x column 1 buttons; ; PA5; ; column 1, with a pull-down resistor
	- 4x column 2 buttons; ; PA6; ; column 2, with a pull-down resistor
	- 4x column 3 buttons; ; PA15; ; column 3, with a pull-down resistor
- 1x piezo-electric element (buzzer, without driver)
	- pin 1; ; PA7; TIM1_CH1; PWM with 4 kHz used
	- pin 2; ; PA8; TIM1_CH1N; PWM with 4 kHz used

All pins are configured using `define`s in the corresponding source code.

code
====

dependencies
------------

The source code uses the [libopencm3](http://libopencm3.org/) library.
The projects is already a git submodules.
To initialize and it you just need to run once: `git submodule init` and `git submodule update`.

firmware
--------

To compile the firmware run `make`.

documentation
-------------

To generate doxygen documentation run `make doc`.

flash
-----

The firmware will be flashed using SWD (Serial Wire Debug).
For that you need an SWD adapter.
The `Makefile` uses a ST-Link V2 along OpenOCD software (per default), or a Black Magic Probe.
To flash using SWD run `make flash`.

debug
-----

SWD also allows to debug the code running on the micro-controller using GDB.
To start the debugging session run `make debug`.

USB
---

The firmware offers serial communication over USART1 and USB (using the CDC ACM device class).
Date and time can be set over serial.

You can also reset the board by setting the serial width to 5 bits over USB.
To reset the board run `make reset`.
This only works if the USB CDC ACM is running correctly and the micro-controller isn't stuck.
