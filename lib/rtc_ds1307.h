/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to communicate with the Maxim DS1307 I2C RTC IC (API)
 *  @file rtc_ds1307.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016-2017
 *  @note peripherals used: I2C @ref i2c_master_i2c
 */
#pragma once

/** setup communication with RTC IC
 *  configure the I2C port defined in the sources
 */
void rtc_ds1307_setup(void);
/** verify if oscillator is disabled
 *  @return if oscillator is disabled (or if communication error occurred)
 */
bool rtc_ds1307_oscillator_disabled(void);
/** read square wave output frequency (in Hz)
 *  @return square wave output frequency in Hz, 0 if disabled (0xffff if communication error occurred)
 */
uint16_t rtc_ds1307_read_square_wave(void);
/** read seconds from RTC IC
 *  @return number of seconds (0-59) of the current time (0xff if communication error occurred)
 */
uint8_t rtc_ds1307_read_seconds(void);
/** read minutes from RTC IC
 *  @return number of minutes (0-59) of the current time (0xff if communication error occurred)
 */
uint8_t rtc_ds1307_read_minutes(void);
/** read hours from RTC IC
 *  @return number of hours (0-23) of the current time (0xff if communication error occurred)
 */
uint8_t rtc_ds1307_read_hours(void);
/** read day from RTC IC
 *  @return day of the week (1-7, 1 is Sunday) of the current time, 1 being Sunday (0xff if communication error occurred)
 */
uint8_t rtc_ds1307_read_day(void);
/** read date from RTC IC
 *  @return day of the month (1-31) of the current time (0xff if communication error occurred)
 */
uint8_t rtc_ds1307_read_date(void);
/** read month from RTC IC
 *  @return month of the year (1-12) of the current time (0xff if communication error occurred)
 */
uint8_t rtc_ds1307_read_month(void);
/** read year from RTC IC
 *  @return year of the century (00-99) of the current time (0xff if communication error occurred)
 */
uint8_t rtc_ds1307_read_year(void);
/** read time from RTC IC
 *  @return array of {seconds, minutes, hours, day, date, month, year} as defined above (NULL if communication error occurred)
 */
uint8_t* rtc_ds1307_read_time(void);
/** read user RAM from RTC IC
 *  @param[in] start start of the user RAM to read (0-55)
 *  @param[out] data array to store the RAM read
 *  @param[in] length number of user RAM bytes to read (0-55)
 *  @return if read succeeded
 */
bool rtc_ds1307_read_ram(uint8_t start, uint8_t* data, uint8_t length);
/** read ROM from AT24Cxx EEPROM
 *  @note only applies if an AT24Cxx EEPROM is also on the I2C bus, such as on the tiny RTC module
 *  @param[in] start start address of the ROM to read (maximum depends on the EEPROM)
 *  @param[out] data array to store the ROM read
 *  @param[in] length number of bytes to read (maximum depends on the EEPROM)
 *  @return if read succeeded
 */
bool rtc_ds1307_read_rom(uint16_t start, uint8_t* data, uint16_t length);
/** disable RTC IC oscillator
 *  @return if disabling oscillator succeeded
 */
bool rtc_ds1307_oscillator_disable(void);
/** enable RTC IC oscillator
 *  @return if enabling oscillator succeeded
 */
bool rtc_ds1307_oscillator_enable(void);
/** write square wave output frequency (in Hz)
 *  @param[in] frequency square wave output frequency in Hz (0 to disable, 1, 4096, 8192, 32768)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_square_wave(uint16_t frequency);
/** write seconds into RTC IC
 *  @param[in] seconds number of seconds (0-59)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_seconds(uint8_t seconds);
/** write minutes into RTC IC
 *  @param[in] minutes number of minutes (0-59)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_minutes(uint8_t minutes);
/** write hours into RTC IC
 *  @param[in] hours number of hours (0-23)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_hours(uint8_t hours);
/** write day into RTC IC
 *  @param[in] day day of the week (1-7, 1 is Sunday)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_day(uint8_t day);
/** write date into RTC IC
 *  @param[in] date day of the month (1-31)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_date(uint8_t date);
/** write month into RTC IC
 *  @param[in] month month of the year (1-12)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_month(uint8_t month);
/** write year into RTC IC
 *  @param[in] year year of the century (00-99)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_year(uint8_t year);
/** write time into RTC IC
 *  @param[in] seconds number of seconds (0-59)
 *  @param[in] minutes number of minutes (0-59)
 *  @param[in] hours number of hours (0-23)
 *  @param[in] day day of the week (1-7, 1 is Sunday)
 *  @param[in] date day of the month (1-31)
 *  @param[in] month month of the year (1-12)
 *  @param[in] year year of the century (00-99)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_time(uint8_t seconds, uint8_t minutes, uint8_t hours, uint8_t day, uint8_t date, uint8_t month, uint8_t year);
/** write to user RAM on RTC IC
 *  @param[in] start start of the user RAM to write (0-55)
 *  @param[in] data array of byte to write in RAM
 *  @param[in] length number of user RAM bytes to write (0-55)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_ram(uint8_t start, uint8_t* data, uint8_t length);
/** write to ROM on AT24Cxx EEPROM
 *  @note only applies if an AT24Cxx EEPROM is also on the I2C bus, such as on the tiny RTC module
 *  @param[in] start start address of the ROM to write (maximum depends on the EEPROM)
 *  @param[in] data array to write in ROM
 *  @param[in] length number of bytes to write (maximum depends on the EEPROM)
 *  @return if write succeeded
 */
bool rtc_ds1307_write_rom(uint16_t start, uint8_t* data, uint8_t length);
