/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to communicate with a Titan Micro MAX7219 IC attached to a 4-digit 7-segment (code)
 *  @file led_max7219.c
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2017
 *  @note peripherals used: GPIO @ref led_max7219_gpio, timer @ref led_tm1637_timer
 *  @warning all calls are blocking
 *
 *  bit vs segment: 0bpabcdefg
 *  +a+
 *  f b
 *  +g+
 *  e c p
 *  +d+
 */

/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdlib.h> // general utilities
#include <string.h> // string utilities

/* STM32 (including CM3) libraries */
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/spi.h> // SPI library
#include <libopencm3/stm32/timer.h> // timer library

#include "global.h" // global utilities
#include "led_max7219.h" // MAX7219 header and definitions

/** @defgroup led_max7219_gpio GPIO used to control MAX7219 IC load line
 *  @{
 */
#define LED_MAX7219_LOAD_PORT B /**< port for load line */
#define LED_MAX7219_LOAD_PIN 12 /**< pin for load line */
/** @} */

/** @defgroup led_max7219_spi SPI used to communication with MAX7219 IC
 *  @{
 */
#define LED_MAX7219_SPI 2 /**< SPI to send data */
/** @} */

/** ASCII characters encoded for the 7 segments digit block
 *  @note starts with space
 */
static const uint8_t ascii_7segments[] = {
	0x00, // space
	0x06, // ! (I)  
	0x22, // "      
	0x1d, // # (o)  
	0x5b, // $ (s)  
	0x25, // % (/)  
	0x5f, // & (6)  
	0x02, // '      
	0x4e, // ( ([)  
	0x78, // )      
	0x07, // *      
	0x31, // +      
	0x04, // ,      
	0x01, // -      
	0x04, // . (,)  
	0x25, // /      
	0x7e, // 0      
	0x30, // 1      
	0x6d, // 2      
	0x79, // 3      
	0x33, // 4      
	0x5b, // 5      
	0x5f, // 6      
	0x70, // 7      
	0x7f, // 8      
	0x7b, // 9      
	0x09, // : (=)  
	0x09, // ; (=)  
	0x0d, // <      
	0x09, // =      
	0x19, // >      
	0x65, // ?      
	0x6f, // @      
	0x77, // A      
	0x7f, // B      
	0x4e, // C      
	0x3d, // D      
	0x4f, // E      
	0x47, // F      
	0x5e, // G      
	0x37, // H      
	0x06, // I      
	0x3c, // J      
	0x37, // K      
	0x0e, // L      
	0x76, // M      
	0x76, // N      
	0x7e, // O      
	0x67, // P      
	0x6b, // Q      
	0x66, // R      
	0x5b, // S      
	0x0f, // T      
	0x3e, // U      
	0x3e, // V (U)  
	0x3e, // W (U)  
	0x37, // X (H)  
	0x3b, // Y      
	0x6d, // Z      
	0x4e, // [      
	0x13, // '\'    
	0x78, // /      
	0x62, // ^      
	0x08, // _      
	0x20, // `      
	0x7d, // a      
	0x1f, // b      
	0x0d, // c      
	0x3d, // d      
	0x6f, // e      
	0x47, // f      
	0x7b, // g      
	0x17, // h      
	0x04, // i      
	0x18, // j      
	0x37, // k      
	0x06, // l      
	0x15, // m      
	0x15, // n      
	0x1d, // o      
	0x67, // p      
	0x73, // q      
	0x05, // r      
	0x5b, // s      
	0x0f, // t      
	0x1c, // u      
	0x1c, // v (u)  
	0x1c, // w (u)  
	0x37, // x      
	0x3b, // y      
	0x6d, // z      
	0x4e, // { ([)  
	0x06, // |      
	0x78, // } ([)  
	0x01, // ~
};

/** number of display in the chain */
uint8_t lex_max7219_displays = 0;

/** write data on SPI bus and handle load signal
 *  @param[in] data bytes to write
 *  @param[in] display display number in chain (0xff for all)
 */
static void led_max7219_write(uint16_t data, uint8_t display)
{
	if (lex_max7219_displays<=display && 0xff!=display) { // display no in chain
		return;
	}

	gpio_clear(GPIO(LED_MAX7219_LOAD_PORT), GPIO(LED_MAX7219_LOAD_PIN)); // ensure load pin is low (data is put in MAX7219 register on rising edge)
	for (uint8_t i=lex_max7219_displays; i>0; i--) { // go though all displays
		while (SPI_SR(SPI(LED_MAX7219_SPI))&SPI_SR_BSY); // wait until not busy
		if (0xff==display || i==(display+1)) { // right display or broadcast message
			spi_send(SPI(LED_MAX7219_SPI), data); // send data
		} else {
			spi_send(SPI(LED_MAX7219_SPI), 0x0000); // send no-op command to shift command to correct display or out
		}
		while (!(SPI_SR(SPI(LED_MAX7219_SPI))&SPI_SR_TXE)); // wait until Tx is empty (reference manual says BSY should also cover this, but it doesn't)
		while (SPI_SR(SPI(LED_MAX7219_SPI))&SPI_SR_BSY); // wait until not busy (= transmission completed)
	}
	gpio_set(GPIO(LED_MAX7219_LOAD_PORT), GPIO(LED_MAX7219_LOAD_PIN)); // create rising edge on load pin for data to be set in MAX7219 register
}

void led_max7219_setup(uint8_t displays)
{
	// saved number of displays
	lex_max7219_displays = displays;

	// configure GPIO for load line
	rcc_periph_clock_enable(RCC_GPIO(LED_MAX7219_LOAD_PORT)); // enable clock for GPIO peripheral
	gpio_clear(GPIO(LED_MAX7219_LOAD_PORT), GPIO(LED_MAX7219_LOAD_PIN)); // idle low (load on rising edge)
	gpio_set_mode(GPIO(LED_MAX7219_LOAD_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(LED_MAX7219_LOAD_PIN)); // set as output

	// configure SPI peripheral
	rcc_periph_clock_enable(RCC_SPI_SCK_PORT(LED_MAX7219_SPI)); // enable clock for GPIO peripheral for clock signal
	gpio_set_mode(SPI_SCK_PORT(LED_MAX7219_SPI), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, SPI_SCK_PIN(LED_MAX7219_SPI)); // set as output (max clock speed for MAX7219 is 10 MHz)
	rcc_periph_clock_enable(RCC_SPI_MOSI_PORT(LED_MAX7219_SPI)); // enable clock for GPIO peripheral for MOSI signal
	gpio_set_mode(SPI_MOSI_PORT(LED_MAX7219_SPI), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, SPI_MOSI_PIN(LED_MAX7219_SPI)); // set as output
	rcc_periph_clock_enable(RCC_AFIO); // enable clock for SPI alternate function
	rcc_periph_clock_enable(RCC_SPI(LED_MAX7219_SPI)); // enable clock for SPI peripheral
	spi_reset(SPI(LED_MAX7219_SPI)); // clear SPI values to default
	spi_init_master(SPI(LED_MAX7219_SPI), SPI_CR1_BAUDRATE_FPCLK_DIV_8, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_16BIT, SPI_CR1_MSBFIRST); // initialise SPI as master, divide clock by 8 since max MAX7219 clock is 10 MHz and max SPI PCLK clock is 72 Mhz, depending on which SPI is used, set clock polarity to idle low (as in the datasheet of the MAX7219, but not that important), set clock phase to go high when bit is set (depends on polarity) as data is stored on MAX7219 on rising edge), use 16 bits frames (as used by MAX7219), use MSB first
	spi_set_unidirectional_mode(SPI(LED_MAX7219_SPI)); // we only need to transmit data
	spi_enable(SPI(LED_MAX7219_SPI)); // enable SPI
}

void led_max7219_on(uint8_t display)
{
	led_max7219_write(0x0C01, display); // put in normal operation more (registers remain as set)
}

void led_max7219_off(uint8_t display)
{
	led_max7219_write(0x0C00, display); // put in shutdown mode (registers remain as set)
}

void led_max7219_test(bool test, uint8_t display)
{
	if (test) {
		led_max7219_write(0x0F01, display); // go into display test mode
	} else {
		led_max7219_write(0x0F00, display); // go into normal operation mode
	}
}

void led_max7219_intensity(uint8_t intensity, uint8_t digits, uint8_t display)
{
	if (intensity>15) { // intensity must be 0-15 (corresponds to (2*brightness+1)/32)
		return;
	}
	if (digits<1 || digits>8) { // scan limit must bit 0-7
		return;
	}
	led_max7219_write(0x0A00+intensity, display); // set brightness
	led_max7219_write(0x0B00+digits-1, display); // set scan limit to display digits
}

bool led_max7219_text(char* text, uint8_t display)
{
	for (uint8_t i=0; i<8; i++) { // input text should only contain printable character (8th bit is used for dots)
		if ((text[i]&0x7f)<' ' || (text[i]&0x7f)>=' '+LENGTH(ascii_7segments)) {
			return false;
		}
	}
	led_max7219_write(0x0900, display); // disable BCD decoding on all 7 digits
	for (uint8_t i=0;  i<8; i++) { // display text
		 led_max7219_write(((i+1)<<8)+(ascii_7segments[(text[7-i]&0x7f)-' '])+(text[7-i]&0x80), display); // send digit (in reverse order)
	}
	return true;
}

void led_max7219_number(uint32_t number, uint8_t dots, uint8_t display)
{
	led_max7219_write(0x09FF, display); // enable BCD decoding on all 7 digits
	for (uint8_t digit=0; digit<8; digit++) { // go through digits
		if (0==digit) { // display 0 on 0 only to first digit
			led_max7219_write(((digit+1)<<8) + (number%10) + (((dots>>digit)&0x01)<<7), display); // display digit
		} else if (0==number) { // display blank on other digits
			led_max7219_write(((digit+1)<<8) + 0x0F + (((dots>>digit)&0x01)<<7), display); // display blank
		} else {
			led_max7219_write(((digit+1)<<8) + (number%10) + (((dots>>digit)&0x01)<<7), display); // display digit
		}
		number /= 10; // get next digit
	}
}
