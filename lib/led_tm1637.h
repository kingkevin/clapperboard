/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to communicate with a Titan Micro TM1637 IC attached to a 7-segment displays (API)
 *  @file led_tm1637.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2017
 *  @note peripherals used: GPIO @ref led_tm1637_gpio, timer @ref led_tm1637_timer
 *  @note only 4-digit 7-segment displays are considered as this is the most common case
 *  @warning all calls are blocking
 */
#pragma once

/** display brightness levels
 */
enum led_tm1637_brightness_t {
	LED_TM1637_1DIV16 = 0,
	LED_TM1637_2DIV16 = 1,
	LED_TM1637_4DIV16 = 2,
	LED_TM1637_10DIV16 = 3,
	LED_TM1637_11DIV16 = 4,
	LED_TM1637_12DIV16 = 5,
	LED_TM1637_13DIV16 = 6,
	LED_TM1637_14DIV16 = 7,
};

/** setup communication with TM1637 IC
 */
void led_tm1637_setup(void);
/** switch display on
 *  @return if transmission succeeded
 */
bool led_tm1637_off(void);
/** set display brightness
 *  @param[in] brightness brightness level to set
 */
void led_tm1637_brightness(enum led_tm1637_brightness_t brightness);
/** display number
 *  @param[in] number number to display (0-9999)
 *  @return if transmission succeeded
 */
bool led_tm1637_number(uint16_t number);
/** display time
 *  @param[in] hours hours to display (0-99)
 *  @param[in] minutes minutes to display (0-99)
 *  @note display separator between hours and minutes
 *  @return if transmission succeeded
 */
bool led_tm1637_time(uint8_t hours, uint8_t minutes);
/** display text
 *  @param[in] text text to display (4 characters)
 *  @note use first bit of each character to enable dot
 *  @return if transmission succeeded
 */
bool led_tm1637_text(char* text);
