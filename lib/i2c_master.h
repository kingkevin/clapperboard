/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to communicate using I2C as master (API)
 *  @file i2c_master.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2017
 *  @note peripherals used: I2C @ref i2c_master_i2c, timer @ref i2c_master_timer
 */
#pragma once

/** setup I2C peripheral
 *  @param[in] fast use standard (100 kHz) or fast (400 kHz) mode
 */
void i2c_master_setup(bool fast);
/** read from I2C slave
 *  @param[in] slave 7-bit I2C salve device address to read from
 *  @param[in] address memory address of slave to read from
 *  @param[in] address_size address size in bytes
 *  @param[out] data array to store bytes read
 *  @param[in] data_size number of bytes to read
 *  @return if read succeeded
 */
bool i2c_master_read(uint8_t slave, const uint8_t* address, size_t address_size, uint8_t* data, size_t data_size);
/** write to I2C slave
 *  @param[in] slave 7-bit I2C salve device address to write to
 *  @param[in] address memory address of slave to write to
 *  @param[in] address_size address size in bytes
 *  @param[in] data array of byte to write to slave
 *  @param[in] data_size number of bytes to write
 *  @return if write succeeded
 */
bool i2c_master_write(uint8_t slave, const uint8_t* address, size_t address_size, const uint8_t* data, size_t data_size);

